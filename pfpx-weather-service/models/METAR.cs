﻿using RestSharp.Deserializers;

namespace pfpx_weather_service.models
{
    public struct METAR
    {
        [DeserializeAs(Name = "raw_text")]
        public string RawText
        { get; set; }

        [DeserializeAs(Name = "station_id")]
        public string ICAO
        { get; set; }

        [DeserializeAs(Name = "latitude")]
        public decimal Latitude
        { get; set; }

        [DeserializeAs(Name = "longitude")]
        public decimal Longitude
        { get; set; }
    }
}
