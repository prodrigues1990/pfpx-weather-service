﻿namespace pfpx_weather_service.models
{
    public struct WindsAloftObservation
    {
        public short WindDirection
        { get; set; }

        public short WindSpeed
        { get; set; }

        public int Temperature
        { get; set; }
    }
}
