﻿using RestSharp.Deserializers;

namespace pfpx_weather_service.models
{
    public struct TAF
    {
        [DeserializeAs(Name = "raw_text")]
        public string RawText
        { get; set; }

        [DeserializeAs(Name = "station_id")]
        public string ICAO
        { get; set; }
    }
}
