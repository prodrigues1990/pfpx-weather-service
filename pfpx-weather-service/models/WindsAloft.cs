﻿using System;

namespace pfpx_weather_service.models
{
    public struct WindsAloft
    {
        public string ICAO
        { get; set; }

        public string raw_info
        {
            get
            { return ""; }
            set
            {
                string[] array = value.Split('|');
                for (int i = 0; i < array.Length; i++)
                {
                    string[] array2 = array[i].Split(',');
                    WindsAloftObservation windsAloftObservation =
                        default(WindsAloftObservation);
                    windsAloftObservation.Temperature =
                        Convert.ToInt32(array2[1]);
                    windsAloftObservation.WindDirection =
                        Convert.ToInt16(array2[2]);
                    windsAloftObservation.WindSpeed =
                        Convert.ToInt16(array2[3]);
                    switch (array2[0])
                    {
                        case "30":
                            FL30 = windsAloftObservation;
                            break;
                        case "60":
                            FL60 = windsAloftObservation;
                            break;
                        case "90":
                            FL90 = windsAloftObservation;
                            break;
                        case "120":
                            FL120 = windsAloftObservation;
                            break;
                        case "180":
                            FL180 = windsAloftObservation;
                            break;
                        case "240":
                            FL240 = windsAloftObservation;
                            break;
                        case "300":
                            FL300 = windsAloftObservation;
                            break;
                        case "340":
                            FL340 = windsAloftObservation;
                            break;
                        case "390":
                            FL390 = windsAloftObservation;
                            break;
                        case "430":
                            FL430 = windsAloftObservation;
                            break;
                    }
                }
            }
        }

        public WindsAloftObservation FL30
        { get; set; }

        public WindsAloftObservation FL60
        { get; set; }

        public WindsAloftObservation FL90
        { get; set; }

        public WindsAloftObservation FL120
        { get; set; }

        public WindsAloftObservation FL180
        { get; set; }

        public WindsAloftObservation FL240
        { get; set; }

        public WindsAloftObservation FL300
        { get; set; }

        public WindsAloftObservation FL340
        { get; set; }

        public WindsAloftObservation FL390
        { get; set; }

        public WindsAloftObservation FL430
        { get; set; }
    }
}
