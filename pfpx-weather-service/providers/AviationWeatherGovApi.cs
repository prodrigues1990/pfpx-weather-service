﻿using pfpx_weather_service.models;
using RestSharp;
using System;
using System.Collections.Generic;

namespace pfpx_weather_service.providers
{
    public class AviationWeatherGovApi
    {
        private const string BaseUrl = 
            @"https://aviationweather.gov/adds/dataserver_current/current";

        private static T Execute<T>(RestRequest request) where T : new()
        {
            IRestResponse<T> restResponse = new RestClient
            {
                BaseUrl = new Uri(BaseUrl)
            }
            .Execute<T>(request);

            if (restResponse.ErrorException != null)
                throw new ApplicationException(
                    "Unable to load data from Aviation Weather Gov",
                    restResponse.ErrorException);
            return restResponse.Data;
        }

        public static List<METAR> GetMetars()
        {
            return Execute<List<METAR>>(
                new RestRequest("/metars.cache.xml", Method.GET));
        }

        public static List<TAF> GetTafs()
        {
            return Execute<List<TAF>>(
                new RestRequest("/tafs.cache.xml", Method.GET));
        }
    }
}
