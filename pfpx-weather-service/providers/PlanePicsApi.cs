﻿using pfpx_weather_service.models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Text;

namespace pfpx_weather_service.providers
{
    public class PlanePicsApi
    {
        private const string BaseUrl = "http://www.plane-pics.de/fsxwx/data";

        private static IRestResponse Execute(RestRequest request)
        {
            IRestResponse restResponse = new RestClient
            {
                BaseUrl = new Uri(BaseUrl)
            }
            .Execute(request);

            if (restResponse.ErrorException != null)
                throw new ApplicationException(
                    "Unable to load data from Aviation Weather Gov",
                    restResponse.ErrorException);
            return restResponse;
        }

        public static List<WindsAloft> GetWindsAloft()
        {
            IRestResponse restResponse = Execute(
                new RestRequest("/windsaloft/latest.txt.gz", Method.GET));
            List<WindsAloft> list = new List<WindsAloft>();
            using (MemoryStream stream = new MemoryStream(restResponse.RawBytes))
            using (GZipStream gZipStream = new GZipStream(
                stream, CompressionMode.Decompress))
            using (MemoryStream memoryStream = new MemoryStream())
            {
                gZipStream.CopyTo(memoryStream);
                using (StringReader stringReader = new StringReader(
                    Encoding.UTF8.GetString(memoryStream.ToArray())))
                {
                    for (string text = stringReader.ReadLine();
                        text != null;
                        text = stringReader.ReadLine())
                    {
                        string[] array = text.Split(';');
                        WindsAloft item = default(WindsAloft);
                        item.ICAO = array[0];
                        item.raw_info = array[4];
                        list.Add(item);
                    }
                    return list;
                }
            }
        }
    }
}
