﻿using pfpx_weather_service.models;
using pfpx_weather_service.providers;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;

namespace pfpx_weather_service
{
    class Program
    {
        static void Main(string[] args)
        {
            CultureInfo cultureInfo = (CultureInfo)Thread.CurrentThread
                .CurrentCulture.Clone();
            cultureInfo.NumberFormat.NumberDecimalSeparator = ".";
            Thread.CurrentThread.CurrentCulture = cultureInfo;
            List<METAR> metars = AviationWeatherGovApi.GetMetars();
            List<TAF> tafs = AviationWeatherGovApi.GetTafs();
            List<WindsAloft> windsAloft = PlanePicsApi.GetWindsAloft();
            GenerateWxStationListFile(metars,
                Path.Combine(args[0], "wx_station_list.txt"));
            GenerateCurrentWxSnapshotFile(metars, tafs, windsAloft,
                Path.Combine(args[0], "current_wx_snapshot.txt"));
        }

        public static void GenerateWxStationListFile(List<METAR> stations, string filePath)
        {
            using (StreamWriter streamWriter = new StreamWriter(filePath))
                foreach (METAR station in stations)
                    streamWriter.WriteLine("{0},{1:0.000},{2:0.000},0.0",
                        station.ICAO, station.Latitude, station.Longitude);
        }

        public static void GenerateCurrentWxSnapshotFile(List<METAR> metars, List<TAF> tafs, List<WindsAloft> windsAloft, string filePath)
        {
            using (StreamWriter streamWriter = new StreamWriter(filePath))
                foreach (METAR station in metars)
                {
                    List<TAF> tafList = (from t in tafs
                                      where t.ICAO == station.ICAO
                                      select t).ToList();
                    string taf = (tafList.Count == 0) ? "*" : tafList[0].RawText;
                    List<WindsAloft> list2 = (from w in windsAloft
                                              where w.ICAO == station.ICAO
                                              select w).ToList();
                    string text2 = (list2.Count == 0) ? "*" :
                        string.Format("{0:000},{1:000},{2:00.0}/{3:000},{4:000},{5:00.0}/{6:000},{7:000},{8:00.0}/{9:000},{10:000},{11:00.0}/{12:000},{13:000},{14:00.0}/{15:000},{16:000},{17:00.0}/{18:000},{19:000},{20:00.0}/{21:000},{22:000},{23:00.0}/{24:000},{25:000},{26:00.0}/{25:000},{26:000},{27:00.0}",
                        list2[0].FL30.WindDirection, list2[0].FL30.WindSpeed, list2[0].FL30.Temperature,
                        list2[0].FL60.WindDirection, list2[0].FL60.WindSpeed, list2[0].FL60.Temperature,
                        list2[0].FL90.WindDirection, list2[0].FL90.WindSpeed, list2[0].FL90.Temperature,
                        list2[0].FL120.WindDirection, list2[0].FL120.WindSpeed, list2[0].FL120.Temperature,
                        list2[0].FL180.WindDirection, list2[0].FL180.WindSpeed, list2[0].FL180.Temperature,
                        list2[0].FL240.WindDirection, list2[0].FL240.WindSpeed, list2[0].FL240.Temperature,
                        list2[0].FL300.WindDirection, list2[0].FL300.WindSpeed, list2[0].FL300.Temperature,
                        list2[0].FL340.WindDirection, list2[0].FL340.WindSpeed, list2[0].FL340.Temperature,
                        list2[0].FL390.WindDirection, list2[0].FL390.WindSpeed, list2[0].FL390.Temperature,
                        list2[0].FL430.WindDirection, list2[0].FL430.WindSpeed, list2[0].FL430.Temperature);
                    streamWriter.WriteLine("{0}::{1}::{2}::{3}", station.ICAO, station.RawText, taf, text2);
                }
        }
    }
}
